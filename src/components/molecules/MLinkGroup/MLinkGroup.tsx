import { Component, Prop, h } from "@stencil/core";
import ILinkGroup from "./interfaces/ILinkGroup";

@Component({
  tag: "poe-m-link-group",
  styleUrl: "MLinkGroup.scss",
  shadow: false,
})
export class MLinkGroup {
  @Prop() links: string;

  private parsedLinks: ILinkGroup[] = [];

  componentWillLoad(): void {
    this.parsedLinks = JSON.parse(this.links);
    return;
  }

  render() {
    return (
      <div class="m-link-group">
        {this.parsedLinks.map(({ icon, url, label }) => (
          <poe-a-link icon={icon} url={url} label={label}></poe-a-link>
        ))}
      </div>
    );
  }
}
