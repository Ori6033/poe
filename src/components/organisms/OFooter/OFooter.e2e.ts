import { newE2EPage } from "@stencil/core/testing";
import getCurrentYear from "../../../modules/get-current-year";

describe("OFooter", () => {
  const currentYear = getCurrentYear();
  let page: any, component: any;
  beforeEach(async () => {
    page = await newE2EPage();
    await page.setContent(`<poe-o-footer></poe-o-footer>`);
    component = await page.find("poe-o-footer");
  });

  afterEach(() => {
    page = null;
    component = null;
  });

  it("renders", async () => {
    expect(component).toHaveClass("hydrated");
  });

  it("renders changes to the country data", async () => {
    const element = await page.find("poe-o-footer >>> p");
    const country = "Detroit, US";
    const getTextContent = (prop: string) =>
      `Made with 💖 in ${prop} © ${currentYear}`;

    expect(element.textContent).toEqual(getTextContent(""));
    component.setProperty("country", country);
    await page.waitForChanges();
    expect(element.textContent).toEqual(getTextContent(country));
  });

  // Tests need to be fixed needings a unique identifier per item
  xit("renders changes to the name data", async () => {
    const element = await page.find("poe-o-footer >>> .m-link");
    const name = "Ori Morningstar";
    const getTextContent = (prop: string) => `${prop} (external link)`;

    expect(element.textContent).toEqual(getTextContent(""));
    component.setProperty("name", name);
    await page.waitForChanges();
    expect(element.textContent).toEqual(getTextContent(name));
  });

  // Tests need to be fixed needings a unique identifier per item
  xit("renders changes to the url data", async () => {
    const element = await page.find("poe-o-footer >>> a");
    const url = "foo.com";

    expect(element.getAttribute("href")).toBeNull;
    component.setProperty("url", url);
    await page.waitForChanges();
    expect(element.getAttribute("href")).toEqual(url);
  });
});
