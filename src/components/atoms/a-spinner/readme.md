# env-a-spinner

<!-- Auto Generated Below -->

## Properties

| Property              | Attribute    | Description                                | Type     | Default     |
| --------------------- | ------------ | ------------------------------------------ | -------- | ----------- |
| `size`                | `size`       | Modifies the spinner size                  | `string` | `"medium"`  |
| `styleType`           | `style-type` | Changes the background color of the button | `string` | `"primary"` |
| `testid` _(required)_ | `testid`     | id for automation tests                    | `string` | `undefined` |

---

_Built with [StencilJS](https://stenciljs.com/)_
