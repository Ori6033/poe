import { Component, Prop, State, h } from "@stencil/core";
import { oneOfMany } from "../../../modules/validators";
import { generateUUID } from "../../../modules/uuid";
import { solid } from "../../../modules/font-awesome";

@Component({
  tag: "poe-a-input",
  styleUrl: "a-input.scss",
  shadow: false,
})

/**
 * HTML element is used to create interactive controls for web-based forms
 * in order to accept data from the user
 * @class
 * @see [MDN: HTML Element Input](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/text)
 * @see [W3C: Validating Inputs](https://www.w3.org/WAI/tutorials/forms/validation/)
 * @see [Stenciljs: components](https://stenciljs.com/docs/component)
 */
export class AInput {
  /**
   * Automated test id
   */
  @Prop() testid!: string;
  /**
   * The types a input can be defined on the HTML standard
   * @see [MDN: HTML Input Type Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#type)
   */
  @Prop() type: string = "text";
  /**
   * A string specifying a name for the input control.
   * @see [MDN: HTML Element Input Name Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#name)
   */
  @Prop() name!: string;
  /**
   * If the input is required to contain a value
   * @see [MDN: HTML Element Input Required Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#required)
   */
  @Prop() isRequired: boolean = false;
  /**
   * If the input is disabled
   * @see [MDN: HTML Element Input Disabled Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#disabled)
   */
  @Prop() isDisabled: boolean = false;
  /**
   * If the input has a miminum length constraint
   * @see [MDN: HTML Element Input minlength Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#minlength)
   */
  @Prop() maxlength: number | null = null;
  /**
   * If the input has a maximum length constraint
   * @see [MDN: HTML Element Input maxlength Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#maxlength)
   */
  @Prop() minlength: number | null = null;
  /**
   * Placeholder text for the input
   *  @see [MDN: HTML Element Input Placeholder Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#placeholder)
   */
  @Prop() placeholder: string | null = null;
  /**
   * Regex pattern for input validation
   * @see [MDN: HTML Element Input Pattern Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#pattern)
   */
  @Prop() pattern: string | null = null;
  /**
   * Required text for the input label
   */
  @Prop() labelText!: string;
  /**
   * If the label is visible
   * @defaults true
   */
  @Prop() isLabelVisible: boolean = true;
  /**
   * Any default value to be passed to the input
   * @see [MDN: HTML Element Input Value Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#value)
   */
  @Prop() value: string | null = null;
  /**
   * Specify permissions the user agent has to provide automated assistance in filling the forms
   * @see [HTML Element Input autocomplete Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete)
   */
  @Prop() autocomplete: string = "on";
  /**
   * Used to alter the input type when necessary, for example password inputs
   * @see [CSS Tricks: The options for password revealing inputs](https://css-tricks.com/the-options-for-password-revealing-inputs/)
   */
  @State() inputType: string = this.type;

  private id: string | null = null;

  /**
   * Validates the different props
   * @private
   * @function validateProps
   * @throws displays an error on the console on which prop is invalid
   */
  private validateProps(): void {
    try {
      oneOfMany(this.type, ["text", "password", "email"]);
    } catch (error) {
      return console.error(error);
    }
  }

  /**
   * Changes the input type from password to text and vice versa to show/hide the password
   * @private
   * @function togglePasswordVisibility
   */
  private togglePasswordVisibility(): void {
    if (this.inputType === "password") {
      this.inputType = "text";
    } else {
      this.inputType = "password";
    }
  }

  /**
   * Called once just after the component is first connected to the DOM. Since this method is only
   * called once, it's a good place to load data asynchronously and to setup the state without triggering extra re-renders.
   * A promise can be returned, that can be used to wait for the first render().
   * @public
   * @function componentWillLoad
   * @see [StencilJS componentWillLoad Lifecycle Method](https://stenciljs.com/docs/component-lifecycle#componentwillload)
   */
  componentWillLoad(): void {
    this.validateProps();
    this.id = generateUUID();
  }

  /**
   * Renders the UI with the appropriate state
   * @public
   * @function render
   * @see [StencilJS rendering state](https://stenciljs.com/docs/component-lifecycle#rendering-state)
   */
  render() {
    return (
      <div
        data-testid={`poe-${this.testid}-a-input-container`}
        class="poe-a-input-container"
      >
        <label
          class={`poe-a-input-label-${
            this.isDisabled ? "disabled" : "enabled"
          } poe-a-input-label-${this.isLabelVisible ? "visible" : "hidden"}`}
          htmlFor={this.id}
          data-testid={`poe-${this.testid}-a-input-label`}
        >
          <span class="poe-a-input-label-required-decorator">
            {this.isRequired ? "*" : ""}
          </span>
          {this.labelText}
        </label>
        <div class="poe-a-input-wrapper">
          <input
            id={this.id}
            data-testid={this.testid}
            type={this.inputType}
            name={this.name}
            minlength={this.minlength}
            maxlength={this.maxlength}
            disabled={this.isDisabled}
            required={this.isRequired}
            placeholder={this.placeholder}
            pattern={this.pattern}
            value={this.value}
            autocomplete={this.autocomplete}
          />
          {this.type === "password" ? (
            <span
              class="poe-a-input-password-visibility-icon"
              onClick={() => this.togglePasswordVisibility()}
            >
              {this.inputType === "password" ? (
                <img src={solid.eye} />
              ) : (
                <img src={solid.eyeSlash} />
              )}
            </span>
          ) : null}
        </div>
      </div>
    );
  }
}
