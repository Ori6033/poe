# poe-a-input

<!-- Auto Generated Below -->

## Properties

| Property                 | Attribute          | Description                                                                                 | Type      | Default     |
| ------------------------ | ------------------ | ------------------------------------------------------------------------------------------- | --------- | ----------- |
| `autocomplete`           | `autocomplete`     | Specify permissions the user agent has to provide automated assistance in filling the forms | `string`  | `"on"`      |
| `isDisabled`             | `is-disabled`      | If the input is disabled                                                                    | `boolean` | `false`     |
| `isLabelVisible`         | `is-label-visible` | If the label is visible                                                                     | `boolean` | `true`      |
| `isRequired`             | `is-required`      | If the input is required to contain a value                                                 | `boolean` | `false`     |
| `labelText` _(required)_ | `label-text`       | Required text for the input label                                                           | `string`  | `undefined` |
| `maxlength`              | `maxlength`        | If the input has a miminum length constraint                                                | `number`  | `null`      |
| `minlength`              | `minlength`        | If the input has a maximum length constraint                                                | `number`  | `null`      |
| `name` _(required)_      | `name`             | A string specifying a name for the input control.                                           | `string`  | `undefined` |
| `pattern`                | `pattern`          | Regex pattern for input validation                                                          | `string`  | `null`      |
| `placeholder`            | `placeholder`      | Placeholder text for the input                                                              | `string`  | `null`      |
| `testid` _(required)_    | `testid`           | Automated test id                                                                           | `string`  | `undefined` |
| `type`                   | `type`             | The types a input can be defined on the HTML standard                                       | `string`  | `"text"`    |
| `value`                  | `value`            | Any default value to be passed to the input                                                 | `string`  | `null`      |

---

_Built with [StencilJS](https://stenciljs.com/)_
