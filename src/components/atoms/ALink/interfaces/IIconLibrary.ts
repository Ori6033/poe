import IIconEntry from "./IIconEntry";

interface IIconLibrary {
  [key: string]: IIconEntry;
}

export default IIconLibrary;
