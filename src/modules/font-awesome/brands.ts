import gitlab from "@fortawesome/fontawesome-free/svgs/brands/gitlab.svg";
import github from "@fortawesome/fontawesome-free/svgs/brands/github.svg";
import twitter from "@fortawesome/fontawesome-free/svgs/brands/twitter.svg";

export { gitlab, github, twitter };
