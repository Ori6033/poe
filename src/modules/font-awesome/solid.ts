import externalLink from "@fortawesome/fontawesome-free/svgs/solid/arrow-up-right-from-square.svg";
import eye from "@fortawesome/fontawesome-free/svgs/solid/eye.svg";
import eyeSlash from "@fortawesome/fontawesome-free/svgs/solid/eye-slash.svg";
import circleExclamation from "@fortawesome/fontawesome-free/svgs/solid/circle-exclamation.svg";

export { externalLink, eye, eyeSlash, circleExclamation };
