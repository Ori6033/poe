import { v4, validate } from "uuid";

export function generateUUID() {
  try {
    return v4();
  } catch (error) {
    throw new Error(generateErrorMessage(error.message));
  }
}

export function validateUUID(uuid: string) {
  try {
    return validate(uuid);
  } catch (error) {
    throw new Error(generateErrorMessage(error.message));
  }
}

export function generateErrorMessage(errorMessage: string): string {
  return `[uuid]: ${errorMessage}`;
}
