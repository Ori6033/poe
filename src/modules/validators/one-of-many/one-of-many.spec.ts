import { oneOfMany, generateErrorMessage } from "./one-of-many";

describe("oneOfMany", () => {
  it("returns true when it passes validation", () => {
    expect(oneOfMany(true, [true, false, false, true])).toEqual(true);
    expect(
      oneOfMany("Pikachu", ["Bulbausaur", "Onix", "Charmander", "Pikachu"])
    ).toEqual(true);
    expect(oneOfMany(3, [2, 1, 3, 100])).toEqual(true);
  });

  it("returns error when it fails validation", () => {
    const booleanTest = {
      valueToValidate: true,
      validators: [false, false, false, false],
    };
    expect(() =>
      oneOfMany(booleanTest.valueToValidate, booleanTest.validators)
    ).toThrow(
      generateErrorMessage(booleanTest.valueToValidate, booleanTest.validators)
    );

    const stringTest = {
      valueToValidate: "Pikachu",
      validators: ["Bulbausaur", "Onix", "Charmander", "Raichu"],
    };
    expect(() =>
      oneOfMany(stringTest.valueToValidate, stringTest.validators)
    ).toThrow(
      generateErrorMessage(stringTest.valueToValidate, stringTest.validators)
    );

    const numberTest = {
      valueToValidate: 3,
      validators: [2, 1, 2, 100],
    };
    expect(() =>
      oneOfMany(numberTest.valueToValidate, numberTest.validators)
    ).toThrow(
      generateErrorMessage(numberTest.valueToValidate, numberTest.validators)
    );
  });
});
